# Project for WDG interview

## Levels
Each level is associated to a commit:  
Level 1: the requested function is in the class `User`  
Level 2: You'll find a unit test testing the requested use case in the file `WDGControllerTest` 

## Prerequisite:  
* Maven 3  
* Java 11  

## Building the project

Build the project for sources with the following command:

~~~~
./mvnw clean install  
~~~~

Then run the jar file:  
~~~~ 
java -jar target/interview-wdg-0.0.1-SNAPSHOT.jar 
~~~~


## Authentication

The project uses Basic authentication.

If you are testing with Postman, just go into "Authentication" 
tab under your request and select `Basic Auth` in the `TYPE` menu.

### Credentials
~~~~
username: wdg  
pasword: wdg
~~~~

If you are not using Postman, you need to create a header containing
~~~~
Authorization: Basic d2RnOndkZw== 
~~~~
Where `d2RnOndkZw==` is the username and password separated by a semicolon
and encoded in Base 64  (https://tools.ietf.org/html/rfc7617)

## Usage

### Create Users, Companies and Wallets
**URL** : `/api/create`  
**Method** : `PUT`  
**Header** : `Content-Type: application/json`


**Request Example**

```json
{
  "wallets": [
    {
      "id": 1,
      "name": "gift cards",
      "type": "GIFT"
    },
    {
      "id": 2,
      "name": "food cards",
      "type": "FOOD"
    }
  ],
  "companies": [
    {
      "id": 1,
      "name": "Wedoogift",
      "balance": 1000
    },
    {
      "id": 2,
      "name": "Wedoofood",
      "balance": 3000
    }
  ],
  "users": [
    {
      "id": 1,
      "balance": [
        {
          "wallet_id": 1,
          "amount": 100
        }
      ]
    },
    {
      "id": 2,
      "balance": [
      ]
    },
    {
      "id": 3,
      "balance": []
    }
  ],
  "distributions": [
  ]
}
```


### Performing a single distribution

**URL** : `/api/distribute`  
**Method** : `POST`  
**Header** : `Content-Type: application/json`

**Request Example**
```json
{
      "id": 1,
      "wallet_id": 1,
      "amount": 50,
      "start_date": "2020-09-16",
      "end_date": "2021-09-15",
      "company_id": 1,
      "user_id": 1
}
```

### Performing a set of distributions

**URL** : `/api/distribute/batch`  
**Method** : `POST`  
**Header** : `Content-Type: application/json`

**Request Example**
```json
[
    {
      "id": 1,
      "wallet_id": 1,
      "amount": 50,
      "start_date": "2020-09-16",
      "end_date": "2021-09-15",
      "company_id": 1,
      "user_id": 1
    },
    {
      "id": 2,
      "wallet_id": 1,
      "amount": 100,
      "start_date": "2020-08-01",
      "end_date": "2021-07-31",
      "company_id": 1,
      "user_id": 2
    },
    {
      "id": 3,
      "wallet_id": 1,
      "amount": 1000,
      "start_date": "2020-05-01",
      "end_date": "2021-04-30",
      "company_id": 2,
      "user_id": 3
    },
    {
      "id": 4,
      "wallet_id": 2,
      "amount": 250,
      "start_date": "2020-05-01",
      "end_date": "2021-02-28",
      "company_id": 1,
      "user_id": 1
    }
]
```

### Retrieve all the results (output)

**URL** : `/api/`  
**Method** : `GET`  
**Header** : `Content-Type: application/json`

**Response Example**
```json
{
    "companies": [
        {
            "id": 1,
            "name": "Wedoogift",
            "balance": 600
        },
        {
            "id": 2,
            "name": "Wedoofood",
            "balance": 2000
        }
    ],
    "users": [
        {
            "id": 1,
            "balance": [
                {
                    "wallet_id": 1,
                    "amount": 50
                },
                {
                    "wallet_id": 2,
                    "amount": 0
                }
            ]
        },
        {
            "id": 2,
            "balance": [
                {
                    "wallet_id": 1,
                    "amount": 100
                }
            ]
        },
        {
            "id": 3,
            "balance": [
                {
                    "wallet_id": 1,
                    "amount": 1000
                }
            ]
        }
    ],
    "distributions": [
        {
            "amount": 50,
            "company_id": 1,
            "user_id": 1,
            "start_date": "2020-09-16",
            "end_date": "2021-09-15",
            "wallet_id": 1
        },
        {
            "amount": 100,
            "company_id": 1,
            "user_id": 2,
            "start_date": "2020-08-01",
            "end_date": "2021-07-31",
            "wallet_id": 1
        },
        {
            "amount": 1000,
            "company_id": 2,
            "user_id": 3,
            "start_date": "2020-05-01",
            "end_date": "2021-04-30",
            "wallet_id": 1
        },
        {
            "amount": 250,
            "company_id": 1,
            "user_id": 1,
            "start_date": "2020-05-01",
            "end_date": "2021-02-28",
            "wallet_id": 2
        }
    ]
}
```