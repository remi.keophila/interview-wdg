package com.wdg.interviewwdg;

import com.wdg.interviewwdg.user.UserDTO;
import com.wdg.interviewwdg.wallet.WalletDTO;

import java.util.List;

public class WDGRequest {
    private List<WalletDTO> wallets;
    private List<Company> companies;
    private List<UserDTO> users;

    public List<WalletDTO> getWallets() {
        return wallets;
    }

    public void setWallets(List<WalletDTO> wallets) {
        this.wallets = wallets;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }
}
