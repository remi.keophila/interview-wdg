package com.wdg.interviewwdg;

import com.wdg.interviewwdg.distribution.Distribution;
import com.wdg.interviewwdg.distribution.DistributionService;
import com.wdg.interviewwdg.user.User;
import com.wdg.interviewwdg.user.UserDTO;
import com.wdg.interviewwdg.user.UserWalletDTO;
import com.wdg.interviewwdg.wallet.Wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class WDGService {
    private final DistributionService distributionService;

    List<User> users = new ArrayList<>();
    List<Company> companies = new ArrayList<>();
    List<Wallet> wallets = new ArrayList<>();
    List<Distribution> distributions = new ArrayList<>();

    @Autowired
    public WDGService(DistributionService distributionService) {
        this.distributionService = distributionService;
    }

    public void create(WDGRequest request) {
        this.companies = request.getCompanies();
        List<Wallet> wallets = request.getWallets().stream()
                .map(Wallet::fromDTO)
                .collect(Collectors.toList());
        this.wallets.addAll(wallets);
        List<User> users = createUsers(request);
        this.users.addAll(users);
    }

    private List<User> createUsers(WDGRequest request) {
        return request.getUsers().stream()
                .map(userDTO -> new User(userDTO.getId(), userDTO.getBalance().stream().map(this::getWalletFromDTO).collect(Collectors.toList())))
                .collect(Collectors.toList());
    }

    private Wallet getWalletFromDTO(UserWalletDTO walletDTO) {
        return this.wallets.stream().filter(wallet -> wallet.getId() == walletDTO.getWallet_id()).findFirst().get();
    }

    public void distribute(Distribution distribution) {
        User user = users.stream().filter(u -> u.getId() == distribution.getUser()).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("no user with id "+ distribution.getUser()));
        Company company = companies.stream().filter(c -> c.getId() == distribution.getCompany()).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("no company with id "+ distribution.getCompany()));
        Wallet wallet = wallets.stream().filter(w -> w.getId() == distribution.getWalletId()).findFirst()
                 .orElseThrow(() -> new IllegalArgumentException("no wallet with id "+ distribution.getWalletId()));
        distributionService.distribute(
                company,
                user,
                distribution.getAmount(),
                distribution.getStartDate(),
                distribution.getEndDate(),
                wallet
        );
        distributions.add(distribution);
    }

    public WDGResponse response() {
        return new WDGResponse(
                this.companies,
                this.users.stream().map(user -> new UserDTO(user.getId(), getUserWallets(user))).collect(Collectors.toList()),
                this.distributions
        );
    }

    private List<UserWalletDTO> getUserWallets(User user) {
        List<UserWalletDTO> userWalletDTOS = new ArrayList<>();
        user.getWallets().forEach(wallet -> userWalletDTOS.add(new UserWalletDTO(wallet.getId(), user.balance(wallet.getId()))));
        return userWalletDTOS;
    }

}
