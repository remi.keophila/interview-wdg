package com.wdg.interviewwdg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterviewWdgApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterviewWdgApplication.class, args);
	}

}
