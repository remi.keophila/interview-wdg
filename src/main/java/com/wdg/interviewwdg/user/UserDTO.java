package com.wdg.interviewwdg.user;

import java.util.List;

public class UserDTO {
    private int id;
    private List<UserWalletDTO> balance;

    public UserDTO(int id, List<UserWalletDTO> balance) {
        this.id = id;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<UserWalletDTO> getBalance() {
        return balance;
    }

    public void setBalance(List<UserWalletDTO> balance) {
        this.balance = balance;
    }
}
