package com.wdg.interviewwdg.user;

import com.wdg.interviewwdg.card.Card;
import com.wdg.interviewwdg.wallet.Wallet;

import java.util.ArrayList;
import java.util.List;

public class User {
    private final int id;
    List<Wallet> wallets = new ArrayList<>();
    List<Card> cards = new ArrayList<>();

    public User(int id, List<Wallet> wallets) {
        this.id = id;
        this.wallets.addAll(wallets);
    }

    public int getId() {
        return id;
    }

    public List<Wallet> getWallets() {
        return wallets;
    }

    public boolean hasWallet(int walletId) {
        return wallets.stream().anyMatch(wallet -> wallet.getId() == walletId);
    }

    public void receive(Card card, int walletId) {
        wallets.stream()
                .filter(wallet -> wallet.getId() == walletId)
                .findFirst()
                .ifPresent(wallet -> cards.add(card));
    }

    public int balance(int walletId) {
        Wallet selectedWallet = wallets.stream()
                .filter(wallet -> wallet.getId() == walletId)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);

        return cards.stream()
                .filter(card -> card.getWalletType() == selectedWallet.getType())
                .mapToInt(Card::balance)
                .sum();
    }

    public void addWallet(Wallet wallet) {
        wallets.add(wallet);
    }
}
