package com.wdg.interviewwdg;

import com.wdg.interviewwdg.card.Card;
import com.wdg.interviewwdg.card.FoodCard;
import com.wdg.interviewwdg.card.GiftCard;
import com.wdg.interviewwdg.exception.NotEnoughAmountException;
import com.wdg.interviewwdg.wallet.Wallet;
import com.wdg.interviewwdg.wallet.WalletType;

import java.time.LocalDate;

public class Company {
    private  int id;
    private final String name;
    private int balance;

    public Company(int id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public Card createCard(int amount, LocalDate startDate, LocalDate expirationDate, Wallet wallet) {
        if (balance < amount) {
            throw new NotEnoughAmountException(name + " does not have enough fund to create a "+amount+" gift card");
        }

        this.balance -= amount;
        if(wallet.getType() == WalletType.GIFT) {
            return new GiftCard(amount, startDate, expirationDate);
        } else if (wallet.getType() == WalletType.FOOD) {
            return new FoodCard(startDate, amount);
        }
        throw new IllegalArgumentException("Unknown wallet type " + wallet.getType());
    }

    public int balance() {
        return this.balance;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
