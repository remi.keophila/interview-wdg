package com.wdg.interviewwdg;

import com.google.gson.*;
import com.wdg.interviewwdg.distribution.Distribution;
import com.wdg.interviewwdg.distribution.DistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;


@RestController
public class WDGController {
    @Autowired
    private WDGService wdgService;

    public WDGRequest parse(String inputJson) {
        Gson g = new Gson();
        return g.fromJson(inputJson, WDGRequest.class);
    }

    @PutMapping(value = "/api/create", consumes = "application/json", produces = "application/json")
    public void create(@RequestBody WDGRequest wdgRequest) {
        wdgService.create(wdgRequest);
    }

    @GetMapping(value = "/api/", produces = "application/json")
    public WDGResponse getOutput() {
        return wdgService.response();
    }

    @PostMapping(value = "/api/distribute", consumes = "application/json", produces = "application/json")
    public void distribute(@RequestBody Distribution distribution) {
        wdgService.distribute(distribution);
    }

    @PostMapping(value = "/api/distribute/batch", consumes = "application/json", produces = "application/json")
    public void distributeList(@RequestBody List<Distribution> distributions) {
        distributions.forEach(this::distribute);
    }



    public String level2(String inputJson) {
        WDGService wdgService = new WDGService(new DistributionService());
        WDGRequest wdgRequest = parse(inputJson);
        wdgService.create(wdgRequest);
        List<Distribution> distributions = Arrays.asList(
                new Distribution(
                        1,
                        1,
                        1,
                        LocalDate.of(2020, 8, 1),
                        LocalDate.of(2021, 9, 15),
                        50,
                        1
                ),

                new Distribution(
                        2,
                        1,
                        2,
                        LocalDate.of(2020, 8, 1),
                        LocalDate.of(2021, 7, 31),
                        100,
                        1
                ),

                new Distribution(
                        3,
                        2,
                        3,
                        LocalDate.of(2020, 5 , 1),
                        LocalDate.of(2021, 4, 30),
                        1000,
                        1
                ),

                new Distribution(
                        4,
                        1,
                        1,
                        LocalDate.of(2020, 5 , 1),
                        LocalDate.of(2021, 2, 28),
                        250,
                        2
                )

        );


        distributions.forEach(wdgService::distribute);

        return createJsonString(wdgService.response());
    }

    private String createJsonString(WDGResponse wdgResponse) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(LocalDate.class, new LocalDateAdapter()) // For LocalDate parding
                .create();

        return gson.toJson(wdgResponse);
    }

    private static class LocalDateAdapter implements JsonSerializer<LocalDate> {
        public JsonElement serialize(LocalDate date, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(date.format(DateTimeFormatter.ISO_LOCAL_DATE));
        }
    }
}
