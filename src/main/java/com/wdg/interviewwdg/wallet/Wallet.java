package com.wdg.interviewwdg.wallet;

public class Wallet {
    private final int id;
    private final String name;
    private final WalletType type;

    public Wallet(int id, String name, WalletType type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public static Wallet fromDTO(WalletDTO walletDTO) {
        return new Wallet(
                walletDTO.getId(),
                walletDTO.getName(),
                WalletType.valueOf(walletDTO.getType())
        );
    }

    public WalletType getType() {
        return this.type;
    }

    public int getId() {
        return this.id;
    }

}
