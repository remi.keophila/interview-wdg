package com.wdg.interviewwdg;

import com.wdg.interviewwdg.distribution.Distribution;
import com.wdg.interviewwdg.user.UserDTO;

import java.util.List;

public class WDGResponse {
    private List<Company> companies;
    private List<UserDTO> users;
    private List<Distribution> distributions;

    public WDGResponse(List<Company> companies, List<UserDTO> users, List<Distribution> distributions) {
        this.companies = companies;
        this.users = users;
        this.distributions = distributions;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }

    public List<Distribution> getDistributions() {
        return distributions;
    }

    public void setDistributions(List<Distribution> distributions) {
        this.distributions = distributions;
    }
}
