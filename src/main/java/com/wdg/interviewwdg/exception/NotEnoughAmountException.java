package com.wdg.interviewwdg.exception;

public class NotEnoughAmountException extends RuntimeException {
    public NotEnoughAmountException() {
        super();
    }
    public NotEnoughAmountException(String message, Throwable cause) {
        super(message, cause);
    }
    public NotEnoughAmountException(String message) {
        super(message);
    }
    public NotEnoughAmountException(Throwable cause) {
        super(cause);
    }

}
