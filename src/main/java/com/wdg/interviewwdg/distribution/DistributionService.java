package com.wdg.interviewwdg.distribution;

import com.wdg.interviewwdg.Company;
import com.wdg.interviewwdg.card.Card;
import com.wdg.interviewwdg.exception.NotEnoughAmountException;
import com.wdg.interviewwdg.user.User;
import com.wdg.interviewwdg.wallet.Wallet;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class DistributionService {
    public void distribute(Company company, User user, int amount, LocalDate startDate, LocalDate endDate, Wallet wallet) throws NotEnoughAmountException {
        Card card = company.createCard(
                amount,
                startDate,
                endDate,
                wallet
        );

        if (!user.hasWallet(wallet.getId())) {
            user.addWallet(wallet);
        }

        user.receive(card, wallet.getId());
    }
}
