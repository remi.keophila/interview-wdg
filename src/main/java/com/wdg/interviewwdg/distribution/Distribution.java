package com.wdg.interviewwdg.distribution;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class Distribution {
    private final int id;

    @JsonProperty("company_id")
    private final int company;

    @JsonProperty("user_id")
    private final int user;

    @JsonProperty("start_date")
    private final LocalDate startDate;

    @JsonProperty("end_date")
    private final LocalDate endDate;

    @JsonProperty("wallet_id")
    private final int walletId;
    private final int amount;

    public Distribution(int id, int companyId, int userId, LocalDate startDate, LocalDate endDate, int amount, int walletId) {
        this.id = id;
        this.company = companyId;
        this.user = userId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.amount = amount;
        this.walletId = walletId;
    }

    public int getCompany() {
        return company;
    }

    public int getUser() {
        return user;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public int getAmount() {
        return amount;
    }

    public int getWalletId() {
        return walletId;
    }
}
