package com.wdg.interviewwdg.card;

import com.wdg.interviewwdg.wallet.WalletType;

public interface Card {
    WalletType getWalletType();
    int balance();
}
