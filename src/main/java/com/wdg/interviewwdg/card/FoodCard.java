package com.wdg.interviewwdg.card;

import com.wdg.interviewwdg.wallet.WalletType;

import java.time.Clock;
import java.time.LocalDate;

public class FoodCard implements Card {
    private final LocalDate startDate;
    private final LocalDate endDate;
    private int balance;
    private final Clock clock;

    public FoodCard(LocalDate startDate, int balance, Clock clock) {
        this.balance = balance;
        this.startDate = startDate;
        this.clock = clock;
        if(startDate.isBefore(LocalDate.of(startDate.getYear(), 2, 28))) {
            this.endDate = LocalDate.of(startDate.getYear(), 2, 28);
        } else {
            this.endDate = LocalDate.of(startDate.getYear() + 1, 2, 28);
        }
    }

    public FoodCard(LocalDate startDate, int balance) {
        this(startDate, balance, Clock.systemDefaultZone());
    }

    @Override
    public WalletType getWalletType() {
        return WalletType.FOOD;
    }

    @Override
    public int balance() {
        LocalDate now = LocalDate.now(clock);
        if (now.isAfter(endDate) || now.isBefore(startDate)) {
            return 0;
        }
        return balance;
    }
}
