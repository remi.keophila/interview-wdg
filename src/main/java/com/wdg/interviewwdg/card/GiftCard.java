package com.wdg.interviewwdg.card;

import com.wdg.interviewwdg.wallet.WalletType;

import java.time.LocalDate;

public class GiftCard implements Card {
    private final LocalDate startDate;
    private final LocalDate expirationDate;
    private int balance;

    public GiftCard(int amount, LocalDate startDate, LocalDate expirationDate) {
        this.startDate = startDate;
        this.expirationDate = expirationDate;
        this.balance = amount;
    }

    @Override
    public WalletType getWalletType() {
        return WalletType.GIFT;
    }

    @Override
    public int balance() {
        LocalDate today = LocalDate.now();
        if(today.isAfter(expirationDate) || today.isBefore(startDate)) {
            return 0;
        }
        return balance;
    }
}
