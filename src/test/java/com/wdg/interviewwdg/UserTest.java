package com.wdg.interviewwdg;

import com.wdg.interviewwdg.card.GiftCard;
import com.wdg.interviewwdg.user.User;
import com.wdg.interviewwdg.wallet.Wallet;
import com.wdg.interviewwdg.wallet.WalletType;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Collections;

import static org.junit.Assert.*;

public class UserTest {

    @Test
    public void receive100eurosCard_userWithNoInitialBalance_balanceShouldBe100() {
        User user = new User(1,
                Collections.singletonList(new Wallet(1, "gift", WalletType.GIFT))
        );

        user.receive(new GiftCard(100, LocalDate.now(), LocalDate.now().plusDays(1)), 1);
        assertEquals(100, user.balance(1));
    }

    @Test
    public void receiveExpired100Card_balanceShouldBe0() {
        User user = new User(1,
                Collections.singletonList(new Wallet(1, "gift", WalletType.GIFT))
        );

        user.receive(new GiftCard(100, LocalDate.now(), LocalDate.now().minusDays(1)), 1);
        assertEquals(0, user.balance(1));
    }

    @Test
    public void userHasWallet_hasWallet1_ShouldReturnTrue() {
        User user = new User(1,
                Collections.singletonList(new Wallet(1, "gift", WalletType.GIFT))
        );

        assertTrue(user.hasWallet(1));
    }

    @Test
    public void userHasWallet_hasWallet2_ShouldReturnFalse() {
        User user = new User(1,
                Collections.singletonList(new Wallet(1, "gift", WalletType.GIFT))
        );

        assertFalse(user.hasWallet(2));
    }
}
