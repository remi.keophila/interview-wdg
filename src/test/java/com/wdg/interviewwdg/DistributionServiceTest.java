package com.wdg.interviewwdg;

import com.wdg.interviewwdg.distribution.DistributionService;
import com.wdg.interviewwdg.exception.NotEnoughAmountException;
import com.wdg.interviewwdg.user.User;
import com.wdg.interviewwdg.wallet.Wallet;
import com.wdg.interviewwdg.wallet.WalletType;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class DistributionServiceTest {

    @Test
    public void distribute50eurosGiftCard_And60EurosFoodVoucher_balancesShouldBeOk() {
        Company company = new Company(1, "Wedoogift", 850);
        Wallet giftWallet = new Wallet(1, "gift", WalletType.GIFT);
        Wallet foodWallet = new Wallet(2, "food", WalletType.FOOD);
        User user = new User(1,
                Arrays.asList(giftWallet, foodWallet)
        );

        DistributionService distributionService = new DistributionService();
        distributionService.distribute(
                company,
                user,
                50,
                LocalDate.parse("2020-09-16"),
                LocalDate.parse("2021-09-15"), giftWallet);

        distributionService.distribute(
                company,
                user,
                60,
                LocalDate.now(),
                LocalDate.parse("2021-01-16"), foodWallet);


        assertEquals(50, user.balance(1));
        assertEquals(60, user.balance(2));
        assertEquals(740, company.balance());
    }

    @Test(expected = NotEnoughAmountException.class)
    public void distribute50euros_companyDoesNotHaveFund_ShouldThrowException() {
        Company company = new Company(1, "Wedoogift", 0);
        Wallet wallet = new Wallet(1, "gift", WalletType.GIFT);
        User user = new User(1,
                Collections.singletonList(wallet)
        );

        DistributionService distributionService = new DistributionService();
        distributionService.distribute(
                company,
                user,
                50,
                LocalDate.parse("2020-09-16"),
                LocalDate.parse("2021-09-15"),
                wallet);

    }

}
