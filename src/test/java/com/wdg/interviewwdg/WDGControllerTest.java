package com.wdg.interviewwdg;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class WDGControllerTest {

    @Test
    public void level2() throws IOException {
        String path = this.getClass().getClassLoader().getResource("input.json").getPath();
        String s = new String(Files.readAllBytes(Paths.get(path)));
        WDGController wdgController = new WDGController();
        String s1 = wdgController.level2(s);

        System.out.println(s1);
    }
}
