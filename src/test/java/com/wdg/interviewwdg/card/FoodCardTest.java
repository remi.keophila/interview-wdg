package com.wdg.interviewwdg.card;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class FoodCardTest {
    private final static LocalDate TODAY_DATE = LocalDate.of(2021, 3, 12);

    @Mock
    private Clock clock;

    @Before
    public void initMocks() {
        Clock fixedClock = Clock.fixed(TODAY_DATE.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
        doReturn(fixedClock.instant()).when(clock).instant();
        doReturn(fixedClock.getZone()).when(clock).getZone();
    }

    @Test
    public void balance_NotExpired_ShouldReturnCorrectBalance() {
        FoodCard foodCard = new FoodCard(LocalDate.of(2021, 3, 10), 50, clock);
        assertEquals(50, foodCard.balance());
    }

    @Test
    public void balance_StartDateExpiredFromThisYear_ShouldReturn0() {
        FoodCard foodCard = new FoodCard(LocalDate.of(2021, 1, 10), 50, clock);
        assertEquals(0, foodCard.balance());
    }

    @Test
    public void balance_StartDateExpiredFromLastYear_ShouldReturn0() {
        FoodCard foodCard = new FoodCard(LocalDate.of(2020, 1, 10), 50, clock);
        assertEquals(0, foodCard.balance());
    }
}
