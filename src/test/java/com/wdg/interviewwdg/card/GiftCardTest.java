package com.wdg.interviewwdg.card;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class GiftCardTest {
    @Test
    public void balance_100eurosGiftCard_ShouldBe100() {
        GiftCard giftCard = new GiftCard(100, LocalDate.now(),  LocalDate.now().plusDays(1));
        assertEquals(100, giftCard.balance());
    }

    @Test
    public void balance_expiredGiftCard_ShouldBe0() {
        GiftCard giftCard = new GiftCard(100, LocalDate.now(), LocalDate.now().minusDays(1));
        assertEquals(0, giftCard.balance());
    }
}
